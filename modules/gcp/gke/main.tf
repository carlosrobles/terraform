module "gke" {
  source  = "terraform-google-modules/kubernetes-engine/google//modules/private-cluster"
  version = "~> 12.3.0"

  name       = var.name
  project_id = var.project_id
  region     = var.region

  ip_range_pods     = var.ip_range_pods
  ip_range_services = var.ip_range_services
  network           = var.network
  subnetwork        = var.subnetwork
  regional          = var.regional
  zones             = var.zones

  enable_private_nodes       = true
  http_load_balancing        = var.http_load_balancing
  remove_default_node_pool   = var.remove_default_node_pool

  node_pools = var.node_pools
}
