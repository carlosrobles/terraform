locals {
  name       = "robles-io"
  project_id = "robles-io"
  region     = "us-west1"

  regional          = false
  zones             = ["us-west1-a"]

  http_load_balancing        = true
  remove_default_node_pool   = true

  node_pools = [
    {
      name               = "robles-io"
      machine_type       = "n1-highcpu-2"
      min_count          = 1
      max_count          = 1
      local_ssd_count    = 0
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      preemptible        = true
      initial_node_count = 1
    }
  ]
}

data "terraform_remote_state" "network" {
  backend = "remote"

  config = {
    organization = "robles-io"

    workspaces = {
      name = "gcp-prod-us-west1-network"
    }
  }
}

module "gke" {
  source  = "../../../../../modules/gcp/gke"

  name       = local.name
  project_id = local.project_id
  region     = local.region

  ip_range_pods     = "${data.terraform_remote_state.network.outputs.subnets_names[0]}-secondary"
  ip_range_services = "${data.terraform_remote_state.network.outputs.subnets_names[0]}-secondary"
  network           = data.terraform_remote_state.network.outputs.network_name
  subnetwork        = data.terraform_remote_state.network.outputs.subnets_names[0]
  regional          = local.regional
  zones             = local.zones

  http_load_balancing        = local.http_load_balancing
  remove_default_node_pool   = local.remove_default_node_pool

  node_pools = local.node_pools
}
