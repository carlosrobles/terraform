terraform {
  backend "remote" {
    organization = "robles-io"

    workspaces {
      name = "gcp-prod-us-west1-network"
    }
  }
}
