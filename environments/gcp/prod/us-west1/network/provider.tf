provider "google" {
  version     = "~> 3.52.0"
  project     = local.project_id
  region      = local.region
}
