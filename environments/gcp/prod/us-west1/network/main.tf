locals {
  network_name = "robles-io"
  project_id   = "robles-io"
  region       = "us-west1"

  secondary_ranges = {
    robles-io-us-west1 = [
        {
          range_name    = "robles-io-us-west1-secondary"
          ip_cidr_range = "10.129.10.0/24"
        }
    ]
  }

  subnets = [
    {
      subnet_name   = "robles-io-us-west1"
      subnet_ip     = "10.128.10.0/24"
      subnet_region = "us-west1"
      subnet_private_access = "true"
    }
  ]
}

module "vpc" {
  source  = "../../../../../modules/gcp/network"

  network_name = local.network_name
  project_id   = local.project_id

  secondary_ranges = local.secondary_ranges
  subnets          = local.subnets
}
